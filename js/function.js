
//Main function 
function createReport(){
    var customComposition= document.getElementById("composition_data").value;
    var defaultComposition = [{                              
                                id: 1,
                                type: 'video',
                                name: 'video1',
                                format: 'mpeg',
                                path: 'test/path1/',
                                duration: 5000,
                                position: 1
                            },
                            {
                                id: 5,
                                type: 'text',
                                name: 'text1',
                                value: 'test text1',
                                duration: 1000,
                                position: 2
                            },
                            {
                                id: 15,
                                type: 'audio',
                                name: 'audio1',
                                format: 'mp3',
                                path: 'test/path1/',
                                duration: 3000,
                                position: 3
                            }, {
                                id: 15,
                                type: 'audio',
                                name: 'audio1',
                                format: 'mp4',
                                path: 'test/path1/',
                                duration: 3000,
                                position: 3
                            }, 
                            {
                                id: 12,
                                type: 'video',
                                name: 'video2',
                                format: 'mpeg',
                                path: 'test/path1/',
                                duration: 8000,
                                position: 4
                            },
                            {
                                id: 19,
                                type: 'image',
                                name: 'image1',
                                format: 'jpeg',
                                path: 'test/path1/',
                                duration: 6000,
                                position: 7
                            },
                            {
                                id: 4,
                                type: 'video',
                                name: 'video3',
                                format: 'avi',
                                path: 'test/path1/',
                                duration: 2000,
                                position: 5
                            },
                                {
                                position: 6,
                                type: 'composition',
                                composition: [
                                    {
                                    id: 6,
                                    type: 'image',
                                    name: 'image11',
                                    format: 'jpeg',
                                    path: 'test/path1/',
                                    duration: 6000,
                                    position: 1
                                    },
                                    {
                                    position: 2,
                                    type: 'composition',
                                    composition: [
                                        {
                                        id: 78,
                                        type: 'image',
                                        name: 'image11',
                                        format: 'gif',
                                        path: 'test/path1test/',
                                        duration: 6000,
                                        position: 1
                                        },
                                        {
                                        id: 234,
                                        type: 'audio',
                                        name: 'audio16',
                                        format: 'ogg',
                                        path: 'test/path12/',
                                        duration: 3000,
                                        position: 3
                                        }, 
                                        {
                                        id: 213,
                                        type: 'video',
                                        name: 'video2',
                                        format: 'ogv',
                                        path: 'test/path1/',
                                        duration: 4000,
                                        position: 4
                                        },
                                        {
                                        id: 675,
                                        type: 'text',
                                        name: 'text1',
                                        value: 'test text4',
                                        duration: 1000,
                                        position: 2
                                        },
                                        {
                                        id: 34,
                                        type: 'video',
                                        name: 'video15',
                                        format: 'mp4',
                                        path: 'test/path1/',
                                        duration: 2000,
                                        position: 5
                                        }
                                    ]
                                    },
                                    {
                                    id: 1,
                                    type: 'video',
                                    name: 'video31',
                                    format: 'mpeg',
                                    path: 'test/path1/',
                                    duration: 2000,
                                    position: 3
                                    }
                                ]
                                }
                            ]
        
var result= analysisData(customComposition !== ''? JSON.parse(customComposition):defaultComposition) 
document.getElementById("composition_data").value = '';

}

//function to give the output to the user.  
function analysisData(Compositionarray){
    duration =0;

var Formatcount = getFormatcount(Compositionarray);
var Typecount = getTypecount(Compositionarray);

 var TotalTime = msToTime(getDuration(Compositionarray));
  var  report = document.getElementById('reports').innerHTML = "";
 report +="Duration :" + TotalTime +"<br>";
 report += "<ul class=tree><li class=main-section>Type: <br><ul>"
 for(var key in Formatcount) {
        
            report += "<li>"+key +" : "+Typecount[key]+"<br> <ul>";
            report += "<li>Format:</li> <ul>";
            var format = Formatcount[key];
            for(var key in format) {      
             report += "<li>"+key+":"+format[key]+"</li>";
    }
             
            report += "</ul></ul></li>";

        }
 report += "</ul> </li></ul>";
  

document.getElementById('reports').innerHTML = report;
 allformat= []
 allType=[];

}

//calculate the format count in the file or json variable
allformat = [];
var duration = 0;
var arraytest = function(type,format){
  if(typeof(allformat[type]) == 'undefined')
    allformat[type] = {};
  if(typeof(format)== 'undefined') return;
  if(typeof(allformat[type][format]) == 'undefined'){
    allformat[type][format] = 0;
  }
  allformat[type][format]++; 
 
  return allformat;
}

//calculate the format count function 
function getFormatcount(data){
for(var i=0;i<data.length;i++){
     if(typeof(data[i].composition) != 'undefined'){
        getFormatcount(data[i].composition);
        }else{
            retdata = arraytest(data[i].type,data[i].format)
            }
        }
return retdata;
}


//calculate the type of the count variable.
allType = [];
var arraytype = function(type,format){

  if(typeof(allType[type]) == 'undefined'){
    allType[type] = 0;
  }
  allType[type]++; 
 
  return allType;
}
//calculate the type count function
function getTypecount(data){
for(var i=0;i<data.length;i++){
     if(typeof(data[i].composition) != 'undefined'){
        getTypecount(data[i].composition);
        }else{
            retdata = arraytype(data[i].type,data[i].format)
            }
        }
return retdata;
}

//calculating the duration of all the data.
var duration = 0; 
function getDuration(data){
    for(var i=0;i<data.length;i++){
     if(typeof(data[i].composition) != 'undefined'){
        getDuration(data[i].composition);
        }else{duration+=data[i].duration;}
        }
return duration;
}

//converting the time.  
function msToTime(duration) {
    var milliseconds = parseInt((duration%1000)/100)
        , seconds = parseInt((duration/1000)%60)
        , minutes = parseInt((duration/(1000*60))%60)
        , hours = parseInt((duration/(1000*60*60))%24);

    hours = (hours < 10) ? "0" + hours : hours;
    minutes = (minutes < 10) ? "0" + minutes : minutes;
    seconds = (seconds < 10) ? "0" + seconds : seconds;

    return hours + ":hours " + minutes + ":minutes " + seconds + ":Second ";
}


//import function
function importCsvdata() {
var files = document.getElementById('selectFiles').files;
if (files.length <= 0) {
return false;
}

var fr = new FileReader();

fr.onload = function(e) { 
console.log(e);
var result = JSON.parse(e.target.result);
var formatted = JSON.stringify(result, null, 2);
    document.getElementById('composition_data').value = formatted;
}

fr.readAsText(files.item(0));
};
